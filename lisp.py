from __future__ import print_function

def parse(code):
    code = "\n".join([l for l in code.split("\n") if not l.startswith("--")])
    expr_list = listexpr(tokenize(code))
    tree = [maketree(expr) for expr in expr_list]
    return tree

OPERATORS = {
    "+": (lambda a,b: a+b),
    "-": (lambda a,b: a-b),
    "/": (lambda a,b: a/b),
    "*": (lambda a,b: a*b),
    "=": (lambda a,b: a==b),
    "!=": (lambda a,b: a!=b),
    "not": (lambda x: not x),
    "and": (lambda a,b: a and b),
    "or": (lambda a,b: a or b),
    "l": (lambda *a:list(a)),
    "t": (lambda *a:a),
    "do": (lambda *a:a[-1]),
}

SPECIAL_FORMS = ["if", "set", "function"]

class Env(object):
    def __init__(self, *args, **kwargs):
        self.global_env = {}
        self.funcs = {}

    def eval_element(self, e):
        if type(e) is list:
            return eval_expr(e, self.global_env)
        else:
            return eval(e, self.global_env)
    def special_form(self, form, args):
        if form == "set":
            name, value = args
            assert name not in OPERATORS
            self.global_env[name] = self.eval_element(value)
        elif form == "function":
            name, arg_list, body = args
            self.funcs[name] = (arg_list, body)
        elif form == "if":
            cond, if_expr, else_expr = args
            if self.eval_element(cond):
                return self.eval_element(if_expr)
            else:
                return self.eval_element(else_expr)

G = Env()

def interpret(code):
    for expr in parse(code):
       eval_expr(expr, G.global_env)

def eval_expr(expr, env):
    func = expr[0]
    args = expr[1:]
    if func in SPECIAL_FORMS:
        return G.special_form(func, args)
    args = [eval_expr(arg, env) if type(arg) is list else eval(arg, env) for arg in args]
    if func in OPERATORS: 
        return OPERATORS[func](*args)
    elif func in G.funcs:
        return eval_expr(G.funcs[func][1], dict(zip(G.funcs[func][0], args )))
    return eval(func, env)(*args)

def tokenize(code):
    code = code.replace("(", " ( ")
    code = code.replace(")", " ) ")
    tokens = []
    token = None
    token_is_string = False 
    for c in code:
        if not token:
            if c not in (" ", "\n"):
                token = c
                token_is_string = (c == '"')
        else:
            if c in (" ", "\n"):
                if token_is_string:
                    token += c
                else:
                    tokens.append(token)
                    token = None
            elif c == '"':
                    token += c
                    tokens.append(token)
                    token = None
            else:
                token += c
    return tokens

def listexpr(tokens):
    i = 0
    expr_list = []
    while i < len(tokens):
        expr = findexpr(tokens[i:])
        expr_list.append(expr)
        i += len(expr)
    return expr_list

def maketree(expr):
    assert expr[0] == "(" and expr[-1] == ")"
    tree = []
    expr = expr[1:-1]
    i = 0
    while i < len(expr):
        token = expr[i]
        if token == "(":
            sub_expr = findexpr(expr[i:])
            tree.append(maketree(sub_expr))
            i += len(sub_expr)
        else:
            tree.append(token)
            i += 1
    return tree

def findexpr(tokens):
    assert tokens[0] == "("
    i = 1
    n = 1 
    expr = ["("]
    while n > 0:
        token = tokens[i]
        expr.append(token)
        if token == "(":
            n += 1
        elif token == ")":
            n -= 1
        i += 1
    return expr 


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        with open(sys.argv[1], "r") as f:
            interpret(f.read())
    else:
        while True:
            interpret(input("> "))
